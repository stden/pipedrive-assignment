const request = require("supertest");
const app = require("../app");

const input = require('./data/input.json');
const expected = require('./data/expected.json');

const uploadData = callback => {
    request(app)
        .get('/database')
        .expect(200)
        .then(() => {
            request(app)
                .post('/addData')
                .send(input)
                .expect(200)
                .then(() => {
                    callback();
                });
        });
};

describe('All tests', () => {
    test('Create database', (done) => {
        request(app)
            .get("/database")
            .expect('Content-Type', /json/)
            .expect(200)
            .then(response => {
                expect(response.body).toEqual({
                    status: 200,
                    data: "Database created"
                });
                done();
            });
    });

    test('addData: input - garbage', (done) => {
        request(app)
            .post('/addData')
            .send('garbage')
            .expect('Content-Type', /json/)
            .expect(400)
            .then(response => {
                expect(response.body).toEqual({
                    status: 400,
                    data: "Error: org_name required: {\"garbage\":\"\"}"
                });
                done();
            });
    });

    test('addData: bad input, no organization name', (done) => {
        let data = {
            "orgname": 'something'
        };
        request(app)
            .post('/addData')
            .send(data)
            .expect('Content-Type', /json/)
            .expect(400)
            .then(response => {
                expect(response.body).toEqual({
                    status: 400,
                    data: "Error: org_name required: {\"orgname\":\"something\"}"
                });
                done();
            });
    });

    test('addData: wrong daughters format', (done) => {
        let data = {
            org_name: 'Test',
            daughters: 1
        };
        request(app)
            .post('/addData')
            .send(data)
            .expect('Content-Type', /json/)
            .expect(400)
            .then(response => {
                expect(response.body).toEqual({
                    status: 400,
                    data: "Error: Daughters not array for: {\"org_name\":\"Test\",\"daughters\":1}"
                });
                done();
            });
    });

    test('addData: bad input, daughters havent organization name', (done) => {
        let data = {
            org_name: 'Parent',
            daughters: ['Child']
        };
        request(app)
            .post('/addData')
            .send(data)
            .expect('Content-Type', /json/)
            .expect(400)
            .then(response => {
                expect(response.body).toEqual({
                    status: 400,
                    data: "Error: org_name required: \"Child\""
                });
                done();
            });
    });

    test('Sample data', (done) => {
        request(app)
            .post('/addData')
            .send(input)
            .expect('Content-Type', /json/)
            .expect(200)
            .then(response => {
                expect(response.body).toEqual({
                    status: 200,
                    data: 'Organizations and relations added'
                });
                done();
            });
    });

    test('getRelations: orgName parameter is required', (done) => {
        request(app)
            .get('/getRelations')
            .expect('Content-Type', /json/)
            .expect(400)
            .then(response => {
                expect(response.body).toEqual({
                    data: "orgName parameter is required",
                    status: 400
                });
                done();
            });
    });

    test('getRelations: empty organization', (done) => {
        request(app)
            .get('/getRelations?orgName=')
            .expect('Content-Type', /json/)
            .expect(400)
            .then(response => {
                expect(response.body).toEqual({
                    data: "orgName parameter is required",
                    status: 400
                });
                done();
            });
    });

    test('getRelations: unknown organization', (done) => {
        request(app)
            .get('/getRelations?orgName=unknown')
            .expect('Content-Type', /json/)
            .expect(200)
            .then(response => {
                expect(response.body).toEqual({
                    data: [],
                    status: 200
                });
                done();
            });
    });

    test('getRelations: sample "Black Banana"', (done) => {
        uploadData(() => {
            request(app)
                .get('/getRelations?orgName=Black%20Banana')
                .expect('Content-Type', /json/)
                .expect(200)
                .then(response => {
                    expect(response.body).toEqual({data: expected, status: 200});
                    done();
                });
        });
    });

    test('getRelations: sample "Black Banana", pagination - page 2', (done) => {
        uploadData(() => {
            request(app)
                .get('/getRelations?orgName=Black%20Banana&page=2')
                .expect('Content-Type', /json/)
                .expect(200)
                .then(response => {
                    expect(response.body).toEqual({data: [], status: 200});
                    done();
                });
        });
    });
});
