module.exports = {
    "root": true,
    "env": {
        "node": true,
        "es6": true,
        "jest/globals": true
    },
    "extends": "eslint:recommended",
    "plugins": [
        "jest"
    ],
    "rules": {
        "comma-dangle": ["error", "never"],
        "indent": ["error", 4,],
        "no-console": ["error"],
        "no-constant-condition": ["error"],
        "no-empty": ["error"],
        "no-octal": ["error"],
        "no-redeclare": ["error"],
        "no-undef": ["error"],
        "no-unreachable": ["error"],
        "no-unused-vars": ["error"],
        "semi": ["error", "always"],
        "valid-typeof": ["error"]
    }
};
