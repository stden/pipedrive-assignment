// Database
const mysql = require('mysql');

const PAGE_SIZE = 20;

// "mysql" - host name for GitLab
const CONFIG = 'mysql://root@mysql/pipedrive?multipleStatements=true';
const con = mysql.createConnection(CONFIG);

module.exports = {
    database: callback => {
        const query =
            `CREATE DATABASE IF NOT EXISTS pipedrive;
DROP TABLE IF EXISTS relation;
DROP TABLE IF EXISTS organization;
CREATE TABLE organization (
  org_name VARCHAR(255) NOT NULL,
  PRIMARY KEY (org_name)
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  DEFAULT COLLATE = utf8_general_ci;
CREATE TABLE relation (
  src  VARCHAR(255)                          NOT NULL,
  type ENUM ('parent', 'daughter', 'sister') NOT NULL,
  dst  VARCHAR(255)                          NOT NULL,
  PRIMARY KEY (src, dst),
  KEY src (src),
  KEY dst (dst)
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  DEFAULT COLLATE = utf8_general_ci;`;

        con.query(query, callback);
    },

    addOrganizations: (names, callback) => {
        const query = `INSERT IGNORE INTO organization (org_name) VALUES ('${names.join("'),('")}');`;
        con.query(query, callback);
    },

    addData: (relations, callback) => {
        let query = '';
        for (let i = 0; i < relations.length; i++) {
            let r = relations[i];
            query += `INSERT IGNORE INTO relation (src,type,dst) VALUES ('${r.src}','${r.type}','${r.dst}');`;
        }
        con.query(query, callback);
    },

    getRelations: (name, page, callback) => {
        const offset = (page - 1) * PAGE_SIZE;

        const query = `SELECT type AS relationship_type, dst AS org_name FROM relation
            WHERE src="${name}" ORDER BY dst ASC
            LIMIT ${PAGE_SIZE} OFFSET ${offset}`;

        con.query(query, callback);
    }
};
