const db = require('./db');

const Combinatorics = require('js-combinatorics');

module.exports = {

    checkTree: function (data) {
        let error = null;

        if (!data.org_name) return 'org_name required: ' + JSON.stringify(data);

        if (data.daughters) {
            if (!Array.isArray(data.daughters)) return 'Daughters not array for: ' + JSON.stringify(data);

            for (const daughter of data.daughters) {
                error = this.checkTree(daughter);
                if (error) {
                    break;
                }
            }
        }

        return error;
    },

    prepare: function (org_name, daughters = [], orgs = new Set(), rels = new Set()) {
        orgs.add(org_name);

        let daughterNames = new Set();
        for (const d of daughters) {
            orgs.add(d.org_name);
            daughterNames.add(d.org_name);

            rels.add({src: org_name, dst: d.org_name, type: 'daughter'});
            rels.add({src: d.org_name, dst: org_name, type: 'parent'});

            if (d.daughters) {
                this.prepare(d.org_name, d.daughters, orgs, rels);
            }
        }

        if (daughterNames.size >= 2) {
            let permutations = Combinatorics.permutation([...daughterNames], 2);
            permutations.forEach(permutation => {
                rels.add({src: permutation[0], dst: permutation[1], type: 'sister'});
            });
        }

        return {organizations: orgs, relations: rels};
    },

    addData: function (data, callback) {
        let validationErr = this.checkTree(data);
        if (validationErr) {
            return callback({status: 400, message: 'Error: ' + validationErr});
        }

        let results = this.prepare(data.org_name, data.daughters);

        db.addOrganizations([...results.organizations], err => {
            if (err) return callback({status: 400, message: 'Organizations insert error: ' + err});

            db.addData([...results.relations], err => {
                if (err) return callback({status: 400, message: 'Relations insert error: ' + err});

                callback(null, 'Organizations and relations added');
            });
        });
    },

    getRelations: function (orgName, page, callback) {
        page = page ? parseInt(page) : 1;

        if (!orgName) {
            return callback({status: 400, message: 'orgName parameter is required'});
        }
        if (!Number.isInteger(page) || page < 1) return callback({
            status: 400,
            message: 'page parameter is invalid'
        });

        db.getRelations(orgName, page, (err, rows) => {
            if (err) {
                return callback({status: 400, message: 'Relations retrieve error: ' + err});
            }
            callback(null, rows);
        });
    },

    database: (callback) => {
        db.database(err => {
            if (err) {
                return callback({status: 500, message: 'Tables cannot be created: ' + err});
            }

            callback(null, 'Database created');
        });
    }
};
