Pipedrive assignment
--------------------

Requirements: 
* Node.js ~9 
* MySQL (host mysql) 

Run locally:
------------

Packages:
```sh
npm install
```

Create database:
```sql
CREATE DATABASE IF NOT EXISTS pipedrive;
```

Run tests
```sh
npm test
```

Structure:
----------

**spec**
* [data/input.json](spec/data/input.json) - input sample data
* [data/expected.json](spec/data/expected.json) - expected sample data
* [app.test.js](spec/app.test.js) - tests (jest)

**src**
* [controller.js](src/controller.js) - business logic
* [db.js](src/db.js) - database queries

**root**
* [app.js](app.js) - express application
* [server.js](server.js) - application entry point