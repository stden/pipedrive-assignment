/*eslint no-console: "off"*/

process.on('uncaughtException', console.error);

const app = require('./app');
const PORT = 8080;

app.listen(PORT, (err) => {
    if (err) {
        console.error('Server listen error: ' + err);
    } else {
        console.info('Server: http://localhost:' + PORT);
    }
});
