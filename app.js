const bodyParser = require('body-parser');
const express = require('express');
const controller = require('./src/controller');
const fs = require("fs");

const app = express();

require('dotenv').load();

app.set('port', process.env.PORT || 8080);

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.setHeader('Content-Type', 'application/json');

    res.apiResponse = (err, data) => {
        let status = err ? (err.status || 500) : 200;
        data = err ? (err.message || err) : data;

        return res.status(status).end(JSON.stringify({status, data}));
    };

    next();
});


// Main page (for testing)
app.get('/', (req, res) => {
    fs.readFile('./views/index.html', "utf8", function (err, data) {
        if (err) {
            return res.send(err);
        }
        res.setHeader('Content-Type', 'text/html');
        res.send(data);
    });
});

app.get('/database', function (req, res) {
    return controller.database(res.apiResponse);
});

app.post('/addData', function (req, res) {
    return controller.addData(req.body, res.apiResponse);
});

app.get('/getRelations', function (req, res) {
    controller.getRelations(req.query.orgName, req.query.page, res.apiResponse);
});

app.use((req, res) => res.apiResponse({status: 404, message: 'Page not found'}));

/*eslint no-unused-vars: "off"*/
app.use((err, req, res, next) => res.apiResponse('Server error. ' + err));

module.exports = app;
